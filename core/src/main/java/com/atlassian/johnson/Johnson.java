package com.atlassian.johnson;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.johnson.config.ConfigurationJohnsonException;
import com.atlassian.johnson.config.DefaultJohnsonConfig;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.config.XmlJohnsonConfig;
import com.atlassian.johnson.event.ApplicationEventCheck;
import com.atlassian.johnson.setup.ContainerFactory;
import com.atlassian.johnson.util.StringUtils;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Singleton class for controlling Johnson.
 * <p>
 * Because Johnson is intended to be a maintenance and application consistency layer around an application, its
 * functionality is exposed statically. This class provides both the mechanisms for initialising and terminating
 * Johnson, as well as for retrieving its configuration and the container of events.
 * <p>
 * Where possible to avoid synchronisation issues, it is preferable to bind Johnson to the lifecycle for the servlet
 * container, using {@link com.atlassian.johnson.context.JohnsonContextListener JohnsonContextListener} to initialise
 * and terminate Johnson.
 *
 * @since 2.0
 */
public class Johnson {

    /**
     * When used in a web environment, the {@link JohnsonConfig} will be exposed in the {@code ServletContext} under
     * an attribute with this key.
     *
     * @since 3.0
     */
    public static final String ATTR_CONFIG = Johnson.class.getName() + ":Config";

    /**
     * When used in a web environment, the {@link JohnsonEventContainer} will be exposed in the {@code ServletContext}
     * under an attribute with this key.
     *
     * @since 3.0
     */
    public static final String ATTR_EVENT_CONTAINER = Johnson.class.getName() + ":EventContainer";

    /**
     * During {@link #initialize(javax.servlet.ServletContext) initialisation}, the {@code ServletContext} is examined
     * for an {@code init-param} with this name. If one is found, it controls the location from which the configuration
     * is loaded. Otherwise, {@link XmlJohnsonConfig#DEFAULT_CONFIGURATION_FILE} is used as the default.
     *
     * @since 3.0
     */
    public static final String PARAM_CONFIG_LOCATION = "johnsonConfigLocation";

    private static final Logger LOG = LoggerFactory.getLogger(Johnson.class);

    private static JohnsonConfig config;
    private static JohnsonEventContainer eventContainer;

    private Johnson() {
        throw new UnsupportedOperationException(getClass().getName() + " should not be instantiated");
    }

    /**
     * Retrieves the statically-bound {@link JohnsonConfig}.
     * <p>
     * Note: If Johnson has not been {@link #initialize(String) initialised}, this method <i>will not</i> initialise
     * it. Before attempting to use Johnson, it is left to the application developer to ensure it has been correctly
     * initialised.
     *
     * @return the working configuration
     * @throws IllegalStateException if {@link #initialize} has not been called
     */
    @Nonnull
    public static JohnsonConfig getConfig() {
        checkState(isInitialized(), "Johnson.getConfig() was called before initialisation");

        return config;
    }

    /**
     * Attempts to retrieve the {@link JohnsonConfig} from the provided {@code ServletContext} under the key
     * {@link #ATTR_CONFIG} before falling back on {@link #getConfig()}.
     * <p>
     * Note: If Johnson has not been {@link #initialize(ServletContext) initialised}, this method <i>will not</i>
     * initialise it. Before attempting to use Johnson, it is left to the application developer to ensure it has
     * been correctly initialised.
     *
     * @param context the servlet context
     * @return the working configuration
     * @throws IllegalStateException if {@link #initialize} has not been called
     */
    @Nonnull
    public static JohnsonConfig getConfig(@Nonnull ServletContext context) {
        Object attribute = requireNonNull(context, "context").getAttribute(ATTR_CONFIG);
        if (attribute != null) {
            return (JohnsonConfig) attribute;
        }
        return getConfig();
    }

    /**
     * Retrieves the statically-bound {@link JohnsonEventContainer}.
     * <p>
     * Note: If Johnson has not been {@link #initialize(String) initialised}, this method <i>will not</i> initialise
     * it. Before attempting to use Johnson, it is left to the application developer to ensure it has been correctly
     * initialised.
     *
     * @return the working event container
     * @throws IllegalStateException if {@link #initialize} has not been called
     * @since 3.0
     */
    @Nonnull
    public static JohnsonEventContainer getEventContainer() {
        checkState(isInitialized(), "Johnson.getEventContainer() was called before initialisation");

        return eventContainer;
    }

    /**
     * Attempts to retrieve the {@link JohnsonEventContainer} from the provided {@code ServletContext} under the key
     * {@link #ATTR_EVENT_CONTAINER} before falling back on the statically-bound instance.
     * <p>
     * Note: If Johnson has not been {@link #initialize(ServletContext) initialised}, this method <i>will not</i>
     * initialise it. Before attempting to use Johnson, it is left to the application developer to ensure it has
     * been correctly initialised.
     *
     * @param context the servlet context
     * @return the working event container
     * @throws IllegalStateException if {@link #initialize} has not been called
     */
    @Nonnull
    public static JohnsonEventContainer getEventContainer(@Nonnull ServletContext context) {
        Object attribute = requireNonNull(context, "context").getAttribute(ATTR_EVENT_CONTAINER);
        if (attribute != null) {
            return (JohnsonEventContainer) attribute;
        }
        return getEventContainer();
    }

    /**
     * Initialises Johnson, additionally binding the {@link JohnsonConfig} and {@link JohnsonEventContainer} to the
     * provided {@code ServletContext} and performing any {@link ApplicationEventCheck}s which have been configured.
     * <p>
     * If the {@link #PARAM_CONFIG_LOCATION} {@code init-param} is set, it is used to determine the location of the
     * Johnson configuration file. Otherwise, {@link XmlJohnsonConfig#DEFAULT_CONFIGURATION_FILE} is assumed.
     * <p>
     * Note: This method is <i>not synchronised</i> and <i>not thread-safe</i>. It is left to the <i>calling code</i>
     * to ensure proper synchronisation. The easiest way to do this is to initialise Johnson by adding the
     * {@link com.atlassian.johnson.context.JohnsonContextListener} to {@code web.xml}.
     *
     * @param context the servlet context
     */
    public static void initialize(@Nonnull ServletContext context) {
        String location = StringUtils.defaultIfEmpty(
                requireNonNull(context, "context").getInitParameter(PARAM_CONFIG_LOCATION),
                XmlJohnsonConfig.DEFAULT_CONFIGURATION_FILE);

        initialize(location);

        context.setAttribute(ATTR_CONFIG, config);
        context.setAttribute(ATTR_EVENT_CONTAINER, eventContainer);

        List<ApplicationEventCheck> checks = config.getApplicationEventChecks();
        for (ApplicationEventCheck check : checks) {
            check.check(eventContainer, context);
        }
    }

    /**
     * Initialises Johnson, loading its configuration from the provided location, and sets the statically-bound
     * instances of {@link JohnsonConfig} and {@link JohnsonEventContainer}.
     * <p>
     * If the location provided is {@code null} or empty, {@link XmlJohnsonConfig#DEFAULT_CONFIGURATION_FILE} is
     * assumed.
     * <p>
     * Note: If the configuration fails to load, {@link DefaultJohnsonConfig} is used to provide defaults. For more
     * information about what those defaults are, see the documentation for that class.
     *
     * @param location the location of the Johnson configuration file
     */
    public static void initialize(@Nullable String location) {
        location = StringUtils.defaultIfEmpty(location, XmlJohnsonConfig.DEFAULT_CONFIGURATION_FILE);

        LOG.debug("Initialising Johnson with configuration from [{}]", location);
        try {
            config = XmlJohnsonConfig.fromFile(location);
        } catch (ConfigurationJohnsonException e) {
            LOG.warn("Failed to load configuration from [" + location + "]", e);
            config = DefaultJohnsonConfig.getInstance();
        }

        ContainerFactory containerFactory = config.getContainerFactory();
        eventContainer = containerFactory.create();
    }

    /**
     * Retrieves a flag indicating whether Johnson has been initialized. Initialization may be performed using
     * {@link #initialize(String)} or {@link #initialize(ServletContext)}; this method will return {@code true}
     * after either.
     *
     * @return {@code true} if Johnson has been initialized; otherwise, {@code false}
     * @since 3.3
     */
    public static boolean isInitialized() {
        return !(config == null || eventContainer == null);
    }

    /**
     * Terminates Johnson, clearing the statically-bound {@link JohnsonConfig} and {@link JohnsonEventContainer}.
     */
    public static void terminate() {
        config = null;
        eventContainer = null;
    }

    /**
     * Terminates Johnson, removing Johnson-related attributes from the provided {@code ServletContext} and clearing
     * the statically-bound {@link JohnsonConfig} and {@link JohnsonEventContainer}.
     *
     * @param context the servlet context
     */
    public static void terminate(@Nonnull ServletContext context) {
        requireNonNull(context, "context");

        terminate();

        context.removeAttribute(ATTR_CONFIG);
        context.removeAttribute(ATTR_EVENT_CONTAINER);
    }
}
