package com.atlassian.johnson;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.johnson.event.Event;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * A default implementation of {@link JohnsonEventContainer} which stores events in a list.
 * <p>
 * Note: This implementation is thread-safe.
 *
 * @since 2.0
 */
@ParametersAreNonnullByDefault
public class DefaultJohnsonEventContainer implements JohnsonEventContainer {

    private final List<Event> events = new CopyOnWriteArrayList<>();

    /**
     * Adds the provided {@link Event} to the list.
     *
     * @param event the event to add
     */
    @Override
    public void addEvent(Event event) {
        events.add(requireNonNull(event, "event"));
    }

    @Override
    public void clear() {
        events.clear();
    }

    @Nonnull
    @Override
    public Optional<Event> firstEvent(Predicate<? super Event> predicate) {
        return stream().filter(predicate).findFirst();
    }

    /**
     * Retrieves an <i>unmodifiable</i> view of the current {@link Event} list.
     *
     * @return an unmodifiable collection of zero or more events
     */
    @Nonnull
    @Override
    public List<Event> getEvents() {
        return Collections.unmodifiableList(events);
    }

    @Nonnull
    @Override
    public Collection<Event> getEvents(Predicate<? super Event> predicate) {
        return stream().filter(predicate).collect(toList());
    }

    @Override
    public boolean hasEvent(Predicate<? super Event> predicate) {
        return !getEvents(predicate).isEmpty();
    }

    /**
     * Retrieves a flag indicating whether there are any {@link Event}s in the list.
     *
     * @return {@code true} if the event list is not empty; otherwise, {@code false}
     */
    @Override
    public boolean hasEvents() {
        return !events.isEmpty();
    }

    /**
     * Removes the provided {@link Event} from the list, if it can be found.
     *
     * @param event the event to remove
     */
    @Override
    public void removeEvent(Event event) {
        events.remove(requireNonNull(event, "event"));
    }

    @Nonnull
    @Override
    public Stream<Event> stream() {
        return events.stream();
    }
}
