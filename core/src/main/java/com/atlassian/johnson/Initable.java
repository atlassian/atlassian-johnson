package com.atlassian.johnson;

import java.util.Map;
import javax.annotation.Nonnull;

/**
 * When implementing Johnson interfaces, this secondary interface can also be implemented to indicate the object needs
 * additional initialisation.
 * <p>
 * This interface can be applied to implementations of:
 * <ul>
 * <li>{@link com.atlassian.johnson.setup.ContainerFactory ContainerFactory}</li>
 * <li>{@link com.atlassian.johnson.event.EventCheck EventCheck} (and its derived interfaces)</li>
 * <li>{@link com.atlassian.johnson.setup.SetupConfig SetupConfig}</li>
 * </ul>
 * This interface is applied by the {@link com.atlassian.johnson.config.JohnsonConfig JohnsonConfig} implementation
 * which is being used. For exact details on its semantics, review the documentation for that class as well.
 */
public interface Initable {

    /**
     * Initialise the object, optionally drawing configuration from the provided {@code Map}. The provided map may be
     * empty, if no parameters were configured, but it will never be {@code null}.
     *
     * @param params a map of additional parameters loaded from the configuration file
     */
    void init(@Nonnull Map<String, String> params);
}
