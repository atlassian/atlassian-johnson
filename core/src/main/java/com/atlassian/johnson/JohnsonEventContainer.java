package com.atlassian.johnson;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.johnson.event.Event;

/**
 * Interface defining a container for Johnson {@link Event}s
 * <p>
 * Johnson maintains <i>exactly one</i> event container, which is accessed statically. As a result, all implementations
 * of this interface are required to be thread-safe. However, because Johnson may be used to filter all requests to the
 * application, implementations should be careful in how they achieve that thread-safety. Using heavy synchronisation
 * techniques may impose significant performance penalties.
 *
 * @since 2.0
 */
@ParametersAreNonnullByDefault
public interface JohnsonEventContainer {

    /**
     * Adds the provided event to the collection.
     *
     * @param event the event to add
     */
    void addEvent(Event event);

    /**
     * Removes all events from this container.
     *
     * @since 3.4
     */
    void clear();

    /**
     * Returns the first event (if any) that satisfies the given predicate.
     *
     * @param predicate the predicate to evaluate
     * @return the first such event in container iteration order
     * @see JohnsonEventContainer#getEvents()
     * @since 3.2
     */
    @Nonnull
    Optional<Event> firstEvent(Predicate<? super Event> predicate);

    /**
     * Retrieves an <i>immutable</i> view of the contained {@link Event}s.
     *
     * @return the current events
     */
    @Nonnull
    Collection<Event> getEvents();

    /**
     * Returns any events that satisfy the given predicate.
     *
     * @param predicate the predicate to evaluate
     * @return any matching events in their original iteration order
     * @since 3.2
     */
    @Nonnull
    Collection<Event> getEvents(Predicate<? super Event> predicate);

    /**
     * Indicates whether this container has any events that satisfy the given predicate.
     *
     * This is equivalent to calling {@link #firstEvent(Predicate)} followed by
     * {@link Optional#isPresent()}.
     *
     * @param predicate the predicate to evaluate
     * @return <code>false</code>if no events match
     * @since 3.2
     */
    boolean hasEvent(Predicate<? super Event> predicate);

    /**
     * Retrieves a flag indicating whether there are {@link Event}s in the container.
     * <p>
     * This can be thought of as a shortcut for {@code !getEvents().isEmpty()}.
     *
     * @return {@code true} if there are events; otherwise, {@code false}
     */
    boolean hasEvents();

    /**
     * Removes the specified {@link Event} from the container, if it can be found.
     * <p>
     * Warning: Due to how {@link Event#equals(Object)} and {@link Event#hashCode()} are implemented, an <i>exact</i>
     * match on every field is required in order to match an event. As a result, it may be necessary to iterate over
     * the {@link #getEvents() events} in the collection and remove the event using the exact instance already in the
     * collection.
     *
     * @param event the event to remove
     */
    void removeEvent(Event event);

    /**
     * Retrieves a stream which can be used to filter, map, sort or otherwise iterate over the {@link Event events}
     * in the container.
     *
     * @return a {@code Stream} over the {@link Event events} in the container
     * @since 3.3
     */
    @Nonnull
    Stream<Event> stream();
}
