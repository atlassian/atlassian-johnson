package com.atlassian.johnson.filters;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.*;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.RequestEventCheck;
import com.atlassian.johnson.setup.SetupConfig;
import com.atlassian.johnson.util.StringUtils;

/**
 * Base class for handling error cases where the application is unavailable to handle normal requests.
 */
public abstract class AbstractJohnsonFilter implements Filter {

    protected static final String TEXT_XML_UTF8_CONTENT_TYPE = "text/xml;charset=utf-8";

    protected FilterConfig filterConfig;
    protected JohnsonConfig config;

    /**
     * This filter checks to see if there are any application consistency errors before any pages are accessed. If there are errors then a redirect to the errors page is made
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String alreadyFilteredKey = getClass().getName() + "_already_filtered";
        if (servletRequest.getAttribute(alreadyFilteredKey) != null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } else {
            servletRequest.setAttribute(alreadyFilteredKey, Boolean.TRUE);
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // get the URI of this request
        String servletPath = getServletPath(request);

        // Get the container for this context and run all of the configured request event checks
        JohnsonEventContainer appEventContainer = getContainerAndRunEventChecks(request);

        SetupConfig setup = config.getSetupConfig();

        // if there are application consistency events then redirect to the errors page
        boolean ignoreUri = ignoreURI(servletPath);
        if (appEventContainer.hasEvents() && !ignoreUri) {
            handleError(appEventContainer, request, response);
        }
        // if application is not setup then send to the Setup Page
        else if (!ignoreUri && !setup.isSetup() && !setup.isSetupPage(servletPath)) {
            handleNotSetup(request, response);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy() {}

    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;

        config = Johnson.getConfig();
    }

    protected JohnsonEventContainer getContainerAndRunEventChecks(HttpServletRequest req) {
        // Get the container for this context
        JohnsonEventContainer appEventContainer = Johnson.getEventContainer(filterConfig.getServletContext());
        // run all of the configured request event checks
        for (RequestEventCheck requestEventCheck : config.getRequestEventChecks()) {
            requestEventCheck.check(appEventContainer, req);
        }
        return appEventContainer;
    }

    /**
     * Retrieves the current request servlet path. Deals with differences between servlet specs (2.2 vs 2.3+)
     * <p>
     * Taken from the Webwork RequestUtils class
     *
     * @param request the request
     * @return the servlet path
     */
    protected static String getServletPath(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        if (!StringUtils.isEmpty(servletPath)) {
            return servletPath;
        }

        String requestUri = request.getRequestURI();
        int startIndex = request.getContextPath().equals("")
                ? 0
                : request.getContextPath().length();
        int endIndex =
                request.getPathInfo() == null ? requestUri.length() : requestUri.lastIndexOf(request.getPathInfo());
        if (startIndex > endIndex) {
            // this should not happen
            endIndex = startIndex;
        }

        return requestUri.substring(startIndex, endIndex);
    }

    protected String getStringForEvents(Collection<Event> events) {
        StringBuilder message = new StringBuilder();
        for (Event event : events) {
            if (message.length() > 0) {
                message.append("\n");
            }
            message.append(event.getDesc());
        }
        return message.toString();
    }

    /**
     * Handles the given request for error cases when there is a Johnson {@link com.atlassian.johnson.event.Event} which
     * stops normal application functioning.
     *
     * @param appEventContainer the JohnsonEventContainer that contains the events.
     * @param servletRequest    the request being directed to the error.
     * @param servletResponse   the response.
     * @throws IOException when the error cannot be handled.
     */
    protected abstract void handleError(
            JohnsonEventContainer appEventContainer,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse)
            throws IOException;

    /**
     * Handles the given request for cases when the application is not yet setup which
     * stops normal application functioning.
     *
     * @param servletRequest  the request being directed to the error.
     * @param servletResponse the response.
     * @throws IOException when the error cannot be handled.
     */
    protected abstract void handleNotSetup(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
            throws IOException;

    protected boolean ignoreURI(String uri) {
        return uri.equalsIgnoreCase(config.getErrorPath()) || config.isIgnoredPath(uri);
    }
}
