/**
 * Atlassian Source Code Template.
 * User: Bobby
 * Date: Apr 8, 2003
 * Time: 9:07:18 AM
 * CVS Revision: $Revision: 1.5 $
 * Last CVS Commit: $Date: 2006/10/09 01:01:38 $
 * Author of last CVS Commit: $Author: bkuo $
 */
package com.atlassian.johnson.filters;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.johnson.JohnsonEventContainer;

/**
 * A filter that handles cases where the application is unable to handle a normal request and redirects to the
 * configured error path so that a nice error page can be provided.
 */
@UnrestrictedAccess
public class JohnsonFilter extends AbstractJohnsonFilter {

    private static final Logger LOG = LoggerFactory.getLogger(JohnsonFilter.class);

    protected void handleError(
            JohnsonEventContainer appEventContainer,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse)
            throws IOException {
        String servletPath = getServletPath(servletRequest);
        String contextPath = servletRequest.getContextPath();
        LOG.info(
                "The application is still starting up, or there are errors. Redirecting request from '{}' to '{}'",
                servletPath,
                config.getErrorPath());

        String nextUrl = servletRequest.getRequestURI();

        if (servletRequest.getQueryString() != null
                && !servletRequest.getQueryString().isEmpty()) {
            nextUrl += "?" + servletRequest.getQueryString();
        }

        String redirectUrl = contextPath + config.getErrorPath() + "?next=" + URLEncoder.encode(nextUrl, "UTF-8");

        servletResponse.sendRedirect(redirectUrl);
    }

    protected void handleNotSetup(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
            throws IOException {
        String servletPath = getServletPath(servletRequest);
        String contextPath = servletRequest.getContextPath();
        LOG.info(
                "The application is not yet setup. Redirecting request from '{}' to '{}'",
                servletPath,
                config.getSetupPath());
        servletResponse.sendRedirect(contextPath + config.getSetupPath());
    }
}
