package com.atlassian.johnson.filters;

import java.io.IOException;
import java.util.function.Predicate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;

/**
 * A handler that returns a no-content temporarily unavailable response suitable for refusing responses when an
 * application is unable to handle normal requests. This is especially useful for cases where the normal response
 * is of an unknown, or dynamic content-type and sending actual content may confuse clients.
 * <p>
 * Example uses include AJAX requests, generated images, pdf, excel and word docs.
 */
public class Johnson503Filter extends AbstractJohnsonFilter {

    private static final Logger log = LoggerFactory.getLogger(Johnson503Filter.class);

    protected void handleError(
            JohnsonEventContainer appEventContainer,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse)
            throws IOException {
        log.info("The application is unavailable, or there are errors.  Returning a temporarily unavailable status.");
        servletResponse.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        if (hasOnlyWarnings(appEventContainer)) {
            servletResponse.setHeader("Retry-After", "30");
        }
        // flushing the writer stops the app server from putting its html message into the otherwise empty response
        servletResponse.getWriter().flush();
    }

    protected void handleNotSetup(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
            throws IOException {
        log.info("The application is not setup.  Returning a temporarily unavailable status.");
        servletResponse.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        // flushing the writer stops the app server from putting its html message into the otherwise empty response
        servletResponse.getWriter().flush();
    }

    private static boolean hasOnlyWarnings(JohnsonEventContainer eventContainer) {
        return eventContainer != null
                && eventContainer.stream()
                        .map(Event::getLevel)
                        .map(EventLevel::getLevel)
                        .allMatch(Predicate.isEqual(EventLevel.WARNING));
    }
}
