package com.atlassian.johnson.context;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.atlassian.johnson.Johnson;

/**
 * Initialises and terminates {@link Johnson} with the servlet container.
 * <p>
 * In web environments, this is the preferred mechanism for managing Johnson's lifecycle, allowing it to be initialised
 * and terminated in a thread-safe context. This listener should be registered before any others except, if applicable,
 * those that setup logging.
 * <p>
 * To use this listener, add the following to web.xml:
 * <pre>
 * <code>&lt;listener&gt;
 *     &lt;listener-class&gt;com.atlassian.johnson.context.JohnsonContextListener&lt;/listener-class&gt;
 * &lt;/listener&gt;
 * </code>
 * </pre>
 * Alternatively, if the application is deployed in a Servlet 3 container such as Tomcat 7, this listener can be
 * registered in a {@code ServletContainerInitializer} using {@code ServletContext.addListener}.
 *
 * @since 2.0
 */
public class JohnsonContextListener implements ServletContextListener {

    /**
     * When a {@code JohnsonContextListener} {@link #register(ServletContext) is registered} with a
     * {@code ServletContext}, this attribute will be used to mark its registration so that multiple
     * listeners will not be registered.
     *
     * @since 3.0
     */
    public static final String ATTR_REGISTERED = JohnsonContextListener.class.getName() + ":Registered";

    /**
     * Registers a {@code JohnsonContextListener} with the provided {@code ServletContext} if such a listener has not
     * already been registered.
     * <p>
     * This method is for use as part of Servlet 3-style {@code ServletContainerInitializer} initialization. Listeners
     * cannot be added to {@code ServletContext} instances after they have already been initialized.
     *
     * @param context the {@code ServletContext} to register a {@link JohnsonContextListener} with
     * @throws IllegalArgumentException      if the provided {@link ServletContext} was not passed to
     *                                       {@link javax.servlet.ServletContainerInitializer#onStartup(java.util.Set, ServletContext)}
     * @throws IllegalStateException         if the {@link ServletContext} has already been initialized
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the
     *                                       {@link ServletContextListener#contextInitialized(ServletContextEvent)} method of a
     *                                       {@link ServletContextListener} that was not either declared in {@code web.xml} or
     *                                       {@code web-fragment.xml} or annotated with {@code &#064;WebListener}
     * @since 3.0
     */
    public static void register(@Nonnull ServletContext context) {
        if (context.getAttribute(ATTR_REGISTERED) == null) {
            context.addListener(new JohnsonContextListener());
            context.setAttribute(ATTR_REGISTERED, Boolean.TRUE);
        }
    }

    /**
     * Terminates {@link Johnson}.
     *
     * @param event the context event
     * @see Johnson#terminate(javax.servlet.ServletContext)
     */
    public void contextDestroyed(@Nonnull ServletContextEvent event) {
        Johnson.terminate(event.getServletContext());
    }

    /**
     * Initialises {@link Johnson}.
     *
     * @param event the context event
     * @see Johnson#initialize(javax.servlet.ServletContext)
     */
    public void contextInitialized(@Nonnull ServletContextEvent event) {
        Johnson.initialize(event.getServletContext());
    }
}
