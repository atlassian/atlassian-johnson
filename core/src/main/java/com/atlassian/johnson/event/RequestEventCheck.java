package com.atlassian.johnson.event;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.johnson.JohnsonEventContainer;

/**
 * A check that is run every HTTP request
 */
public interface RequestEventCheck extends EventCheck {

    void check(@Nonnull JohnsonEventContainer eventContainer, @Nonnull HttpServletRequest request);
}
