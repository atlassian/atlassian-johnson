package com.atlassian.johnson.event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletContext;

import com.atlassian.event.api.EventListener;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;

/**
 * A simple listener which may be used with the Atlassian Events framework to listen for {@link AddEvent add} and
 * {@link RemoveEvent remove} events on the bus, allowing simplified interaction with Johnson.
 *
 * @since 2.0
 */
public class JohnsonEventListener {

    private ServletContext servletContext;

    /**
     * Creates a {@code JohnsonEventListener} which will retrieve the {@link JohnsonEventContainer} from the
     * {@code ServletContext} using {@link Johnson#getEventContainer(ServletContext)}, if one is provided, or
     * using {@link Johnson#getEventContainer()} if not.
     *
     * @param servletContext the servlet context to retrieve the container from, which may not be {@code null}
     */
    public JohnsonEventListener(@Nullable ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Adds the Johnson {@link Event} wrapped by the provided {@link AddEvent}  to the container.
     *
     * @param e the add event
     */
    @EventListener
    public void onAdd(@Nonnull AddEvent e) {
        getContainer().addEvent(e.getEvent());
    }

    /**
     * Attempts to remove the Johnson {@link Event} wrapped by the provided {@link RemoveEvent} from the container.
     *
     * @param e the remove event
     */
    @EventListener
    public void onRemove(@Nonnull RemoveEvent e) {
        getContainer().removeEvent(e.getEvent());
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    private JohnsonEventContainer getContainer() {
        return servletContext == null ? Johnson.getEventContainer() : Johnson.getEventContainer(servletContext);
    }
}
