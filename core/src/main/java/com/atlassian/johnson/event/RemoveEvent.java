package com.atlassian.johnson.event;

import java.util.EventObject;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * An {@code EventObject} indicating the provided {@link Event} should be removed from the Johnson event container.
 *
 * @since 2.0
 */
public class RemoveEvent extends EventObject {

    private final Event event;

    /**
     * Constructs a new {@code RemoveEvent}, setting its source and the Johnson {@link Event} to be removed.
     *
     * @param o     the event source
     * @param event the event to removed
     */
    public RemoveEvent(@Nonnull Object o, @Nonnull Event event) {
        super(o);

        this.event = requireNonNull(event, "event");
    }

    /**
     * Retrieves the Johnson {@link Event} to remove from the container.
     *
     * @return the event to remove
     */
    @Nonnull
    public Event getEvent() {
        return event;
    }
}
