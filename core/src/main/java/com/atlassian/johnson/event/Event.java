package com.atlassian.johnson.event;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class represents an ApplicationEvent
 */
public class Event {

    private final Map<Object, Object> attributes;

    private String date;
    private String desc;
    private String exception;
    private EventType key;
    private EventLevel level;
    private int progress;

    public Event(EventType key, String desc) {
        this(key, desc, null, null);
    }

    public Event(EventType key, String desc, String exception) {
        this(key, desc, exception, null);
    }

    public Event(EventType key, String desc, EventLevel level) {
        this(key, desc, null, level);
    }

    public Event(EventType key, String desc, String exception, EventLevel level) {
        this.desc = desc;
        this.exception = exception;
        this.key = key;
        this.level = level;

        attributes = new HashMap<>();
        date = getFormattedDate();
        progress = -1;
    }

    public static String toString(Throwable t) {
        StringWriter string = new StringWriter();
        PrintWriter writer = new PrintWriter(string);

        writer.println(t);
        t.printStackTrace(writer);
        while ((t = t.getCause()) != null) {
            writer.println("Caused by: " + t);
            t.printStackTrace(writer);
        }
        writer.flush();

        return string.toString();
    }

    public void addAttribute(Object key, Object value) {
        attributes.put(key, value);
    }

    public Object getAttribute(Object key) {
        return attributes.get(key);
    }

    public Map getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return desc;
    }

    public String getException() {
        return exception;
    }

    public EventType getKey() {
        return key;
    }

    public EventLevel getLevel() {
        return level;
    }

    public int getProgress() {
        return progress;
    }

    public boolean hasProgress() {
        return (progress != -1);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setKey(EventType name) {
        this.key = name;
    }

    public void setLevel(EventLevel level) {
        this.level = level;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Event)) {
            return false;
        }

        Event e = (Event) o;
        return Objects.equals(getDate(), e.getDate())
                && Objects.equals(getDesc(), e.getDesc())
                && Objects.equals(getException(), e.getException())
                && Objects.equals(getKey(), e.getKey())
                && Objects.equals(getLevel(), e.getLevel());
    }

    public int hashCode() {
        return Objects.hash(getKey(), getDesc(), getException(), getLevel(), getDate());
    }

    public String toString() {
        return "(Event: Level = " + (getLevel() == null ? "" : getLevel() + " ") + ", Key = "
                + (getKey() == null ? "" : getKey() + " ") + ", Desc = "
                + (getDesc() == null ? "" : getDesc() + " ") + ", Exception = "
                + (getException() == null ? "" : getException() + ")");
    }

    private static String getFormattedDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
}
