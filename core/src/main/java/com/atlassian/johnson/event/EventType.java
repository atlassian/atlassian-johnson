package com.atlassian.johnson.event;

import java.util.Objects;

import com.atlassian.johnson.Johnson;

public class EventType {

    private final String description;
    private final String type;

    public EventType(String type, String description) {
        this.description = description;
        this.type = type;
    }

    public static EventType get(String type) {
        return Johnson.getConfig().getEventType(type);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof EventType) {
            EventType e = (EventType) o;

            return Objects.equals(getDescription(), e.getDescription()) && Objects.equals(getType(), e.getType());
        }

        return false;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public int hashCode() {
        return Objects.hash(getType(), getDescription());
    }

    public String toString() {
        return "(EventType: " + type + ")";
    }
}
