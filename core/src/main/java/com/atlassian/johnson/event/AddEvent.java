package com.atlassian.johnson.event;

import java.util.EventObject;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * An {@code EventObject} indicating the provided {@link Event} should be added to the Johnson event container.
 *
 * @since 2.0
 */
public class AddEvent extends EventObject {

    private final Event event;

    /**
     * Constructs a new {@code AddEvent}, setting its source and the Johnson {@link Event} to be added.
     *
     * @param o     the event source
     * @param event the event to add
     */
    public AddEvent(@Nonnull Object o, @Nonnull Event event) {
        super(o);

        this.event = requireNonNull(event, "event");
    }

    /**
     * Retrieves the Johnson {@link Event} to add to the container.
     *
     * @return the event to add
     */
    @Nonnull
    public Event getEvent() {
        return event;
    }
}
