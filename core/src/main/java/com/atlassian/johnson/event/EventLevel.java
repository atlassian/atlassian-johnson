package com.atlassian.johnson.event;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.johnson.Johnson;

import static java.util.Objects.requireNonNull;

/**
 * The severity of an {@link Event}.
 *
 * Applications using Johnson are free to define their own levels with their own
 * semantics; they can use the constants below or not. However be aware that some
 * of the pre-defined levels below have special meaning to the Johnson framework.
 * For more details, search the source code for their usage.
 */
@ParametersAreNonnullByDefault
public class EventLevel {

    /**
     * This level might indicate a non-fatal error, e.g. one that allows
     * the application to start but with reduced functionality.
     */
    public static final String ERROR = "error";

    /**
     * This level typically means that the application is unable to start.
     */
    public static final String FATAL = "fatal";

    /**
     * This level typically indicates a condition that requires attention
     * but does not materially affect the operation of the application.
     */
    public static final String WARNING = "warning";

    private final String description;
    private final String level;

    /**
     * Constructs a new {@code EventLevel} with the provided {@link #getLevel() level} and {@link #getDescription()
     * description}.
     *
     * @param level the internal name of the level in the Johnson configuration, e.g. {@link #FATAL}
     * @param description a human-readable description of this level
     */
    public EventLevel(String level, String description) {
        this.description = requireNonNull(description, "description");
        this.level = requireNonNull(level, "level");
    }

    /**
     * Returns the configured {@link EventLevel} with the given name, or {@code null} if the requested level has
     * not been configured.
     *
     * @param level the name of the level to find
     * @return the requested level, or {@code null} if the level is not defined by the application
     * @throws IllegalStateException if Johnson has not been initialised
     */
    @Nullable
    public static EventLevel get(String level) {
        return Johnson.getConfig().getEventLevel(level);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof EventLevel) {
            EventLevel e = (EventLevel) o;

            return Objects.equals(getDescription(), e.getDescription()) && Objects.equals(getLevel(), e.getLevel());
        }

        return false;
    }

    @Nonnull
    public String getDescription() {
        return description;
    }

    @Nonnull
    public String getLevel() {
        return level;
    }

    public int hashCode() {
        return Objects.hash(getLevel(), getDescription());
    }

    public String toString() {
        return "(EventLevel: " + level + ")";
    }
}
