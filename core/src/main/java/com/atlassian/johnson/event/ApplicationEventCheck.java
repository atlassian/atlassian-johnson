/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Aug 12, 2004
 * Time: 3:03:08 PM
 */
package com.atlassian.johnson.event;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

import com.atlassian.johnson.JohnsonEventContainer;

/**
 * A check that is run every time the application is started.
 */
public interface ApplicationEventCheck extends EventCheck {

    void check(@Nonnull JohnsonEventContainer eventContainer, @Nonnull ServletContext context);
}
