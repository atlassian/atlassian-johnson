package com.atlassian.johnson.config;

import com.atlassian.johnson.JohnsonException;

/**
 * Thrown if errors are encountered while parsing {@link JohnsonConfig}.
 */
public class ConfigurationJohnsonException extends JohnsonException {

    /**
     * Constructs a new {@code ConfigurationJohnsonException} with the provided message.
     *
     * @param s the exception message
     */
    public ConfigurationJohnsonException(String s) {
        super(s);
    }

    /**
     * Constructs a new {@code ConfigurationJohnsonException} with the provided message and cause.
     *
     * @param s         the exception message
     * @param throwable the cause
     */
    public ConfigurationJohnsonException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
