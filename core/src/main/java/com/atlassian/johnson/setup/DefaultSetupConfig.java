package com.atlassian.johnson.setup;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * A default, empty implementation of {@link SetupConfig} which always indicates the application is setup.
 *
 * @since 2.0
 */
public class DefaultSetupConfig implements SetupConfig {

    /**
     * Always {@code true}.
     *
     * @return {@code true}
     */
    @Override
    public boolean isSetup() {
        return true;
    }

    /**
     * Always {@code false}.
     *
     * @param uri the URI of a web page
     * @return {@code false}
     */
    @Override
    public boolean isSetupPage(@Nonnull String uri) {
        requireNonNull(uri, "uri"); // Impose the proper API semantics

        return false;
    }
}
