package com.atlassian.johnson.util;

import javax.annotation.Nullable;

/**
 * A utility class for common null safe String operations. Currently not using StringUtils from commons-lang as cost
 * of depending on this external library has been judged to be more than the value we currently obtain from it.
 *
 * @since 3.5
 */
public final class StringUtils {

    private StringUtils() {
        throw new UnsupportedOperationException(getClass().getName() + " should not be instantiated");
    }

    public static boolean isBlank(@Nullable String inString) {
        return (inString == null || inString.trim().length() == 0);
    }

    public static boolean isEmpty(@Nullable String inString) {
        return (inString == null || inString.length() == 0);
    }

    public static String defaultIfEmpty(@Nullable String value, @Nullable String defaultValue) {
        return isEmpty(value) ? defaultValue : value;
    }
}
