package com.atlassian.johnson.filters;

import java.io.IOException;
import java.util.Arrays;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;
import com.atlassian.johnson.test.SimpleSetupConfig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JohnsonFilterTest {

    private static final String ALREADY_FILTERED = JohnsonFilter.class.getName() + "_already_filtered";

    private static JohnsonEventContainer container;

    @Mock
    private FilterChain filterChain;

    @Mock
    private FilterConfig filterConfig;

    private JohnsonFilter filter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private ServletContext servletContext;

    @AfterClass
    public static void clearConfig() {
        Johnson.terminate();
    }

    @BeforeClass
    public static void setConfig() {
        Johnson.initialize("test-johnson-config.xml");
        assertTrue(Johnson.isInitialized());

        container = Johnson.getEventContainer();
    }

    @Before
    public void setup() {
        SimpleSetupConfig.IS_SETUP = false;

        when(filterConfig.getServletContext()).thenReturn(servletContext);
        when(request.getContextPath()).thenReturn("");

        // Ensure each test starts with an empty container
        for (Event event : container.getEvents()) {
            container.removeEvent(event);
        }

        filter = new JohnsonFilter();
        filter.init(filterConfig);
    }

    @Test
    public void testErrorNotIgnorableURI() throws Exception {
        container.addEvent(new Event(EventType.get("database"), "foo"));

        when(request.getRequestURI()).thenReturn("somepage.jsp");
        when(request.getServletPath()).thenReturn("somepage.jsp");
        when(request.getQueryString()).thenReturn("");

        filter.doFilter(request, response, filterChain);

        verify(response).sendRedirect(eq("/the/error/path.jsp?next=somepage.jsp"));
    }

    @Test
    public void testErrorsIgnorableUri() throws Exception {
        container.addEvent(new Event(EventType.get("database"), "foo"));

        when(request.getServletPath()).thenReturn("/the/error/path.jsp");

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testNoErrorsAndNotSetup() throws Exception {
        when(request.getServletPath()).thenReturn("somepage.jsp");

        filter.doFilter(request, response, filterChain);

        verify(response).sendRedirect(eq("/the/setup/path.jsp"));
    }

    @Test
    public void testNoErrorsSetup() throws Exception {
        SimpleSetupConfig.IS_SETUP = true;

        when(request.getServletPath()).thenReturn("somepage.jsp");

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testNoErrorsSetupButSetupURI() throws Exception {
        SimpleSetupConfig.IS_SETUP = true;

        when(request.getServletPath()).thenReturn("/setuppage.jsp");

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testNoErrorsNotSetupButSetupURI() throws Exception {
        when(request.getServletPath()).thenReturn("/setup.jsp");

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testNoErrorsNotSetupIgnorableURI() throws Exception {
        when(request.getServletPath()).thenReturn("/ignore/path/1.jsp");

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testNoErrorsNotSetupNotIgnorableURI() throws Exception {
        when(request.getServletPath()).thenReturn("/somepage.jsp");

        filter.doFilter(request, response, filterChain);

        verify(response).sendRedirect(eq("/the/setup/path.jsp"));
    }

    @Test
    public void testNotAppliedTwice() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("/somepage.jsp");

        filter.doFilter(request, response, filterChain);

        verify(response).sendRedirect(eq("/the/setup/path.jsp"));

        reset(filterChain, request);
        when(request.getAttribute(eq(ALREADY_FILTERED))).thenReturn(Boolean.TRUE);

        filter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(same(request), same(response));
    }

    @Test
    public void testEmptyServletPaths() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("");
        when(request.getContextPath()).thenReturn("");
        when(request.getRequestURI()).thenReturn("/requestpage.jsp");
        when(request.getPathInfo()).thenReturn(null);

        filter.doFilter(request, response, filterChain);

        verify(response).sendRedirect(eq("/the/setup/path.jsp"));
    }

    @Test
    public void testGetStringForEvents() {
        EventType type = new EventType("mytype", "mydesc");
        Event[] eventsArr = new Event[] {new Event(type, "Error 1"), new Event(type, "Error 2")};
        String stringForEvents = filter.getStringForEvents(Arrays.asList(eventsArr));
        assertEquals("Error 1\nError 2", stringForEvents);
    }

    @Test
    public void testDestinationPathWithQueryParams() throws Exception {
        container.addEvent(new Event(EventType.get("database"), "foo"));

        when(request.getRequestURI()).thenReturn("/the/page/we/want");
        when(request.getQueryString()).thenReturn("query=somequery&query2=somequery2");

        filter.doFilter(request, response, filterChain);

        verify(response)
                .sendRedirect(
                        eq(
                                "/the/error/path.jsp?next=%2Fthe%2Fpage%2Fwe%2Fwant%3Fquery%3Dsomequery%26query2%3Dsomequery2"));
    }
}
