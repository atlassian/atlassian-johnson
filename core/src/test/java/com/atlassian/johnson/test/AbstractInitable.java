package com.atlassian.johnson.test;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.johnson.Initable;

public abstract class AbstractInitable implements Initable {

    public void init(@Nonnull Map<String, String> params) {}
}
