package com.atlassian.johnson.test;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.ApplicationEventCheck;

public class SimpleApplicationEventCheck extends SimpleEventCheck implements ApplicationEventCheck {

    public void check(@Nonnull JohnsonEventContainer eventContainer, @Nonnull ServletContext context) {}
}
