package com.atlassian.johnson.test;

import javax.annotation.Nonnull;

import com.atlassian.johnson.setup.SetupConfig;

import static java.util.Objects.requireNonNull;

public class SimpleSetupConfig extends AbstractInitable implements SetupConfig {

    public static boolean IS_SETUP = false;

    public boolean isSetup() {
        return IS_SETUP;
    }

    public boolean isSetupPage(@Nonnull String uri) {
        return requireNonNull(uri, "uri").contains("setup");
    }
}
