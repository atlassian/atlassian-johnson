package com.atlassian.johnson.test;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.RequestEventCheck;

public class SimpleRequestEventCheck extends SimpleEventCheck implements RequestEventCheck {

    public void check(@Nonnull JohnsonEventContainer eventContainer, @Nonnull HttpServletRequest request) {}
}
