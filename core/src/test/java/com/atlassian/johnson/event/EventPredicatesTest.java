package com.atlassian.johnson.event;

import java.util.function.Predicate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.johnson.Johnson;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.johnson.event.EventLevels.*;

public class EventPredicatesTest {

    private static final String ATTRIBUTE_NAME = "foo";

    @Before
    public void initializeJohnson() {
        Johnson.initialize("test-johnson-config.xml");
    }

    @After
    public void terminateJohnson() {
        Johnson.terminate();
    }

    @Test
    public void testAttributeBasedPredicateWithNonNullValueShouldOnlyMatchEventsWithThatAttributeValue() {
        Predicate<Event> predicate = EventPredicates.attributeEquals(ATTRIBUTE_NAME, "bar");

        assertThat(predicate.test(eventWithAttribute("bar")), is(true));
        assertThat(predicate.test(eventWithAttribute(null)), is(false));
    }

    @Test
    public void testAttributeBasedPredicateWithNullValueShouldOnlyMatchEventsWithNoSuchAttribute() {
        Predicate<Event> predicate = EventPredicates.attributeEquals(ATTRIBUTE_NAME, null);

        assertThat(predicate.test(eventWithAttribute(null)), is(true));
        assertThat(predicate.test(eventWithAttribute("bar")), is(false));
    }

    @Test
    public void testWarningOrErrorPredicateShouldOnlyMatchEventsWithEitherOfThoseLevels() {
        Predicate<Event> predicate = EventPredicates.level(error(), warning());

        assertThat(predicate.test(eventWithLevel(error())), is(true));
        assertThat(predicate.test(eventWithLevel(warning())), is(true));
        assertThat(predicate.test(eventWithLevel(fatal())), is(false));
    }

    @Test
    public void testTypeBasedPredicateShouldOnlyMatchEventsOfThatType() {
        EventType matchingEventType = EventType.get("database");
        EventType nonMatchingEventType = EventType.get("upgrade");

        Predicate<Event> predicate = EventPredicates.type(matchingEventType);

        assertThat(predicate.test(eventWithType(matchingEventType)), is(true));
        assertThat(predicate.test(eventWithType(nonMatchingEventType)), is(false));
    }

    private static Event eventWithAttribute(Object value) {
        Event event = mock(Event.class);
        when(event.getAttribute(ATTRIBUTE_NAME)).thenReturn(value);

        return event;
    }

    private static Event eventWithLevel(EventLevel eventLevel) {
        Event event = mock(Event.class);
        when(event.getLevel()).thenReturn(eventLevel);

        return event;
    }

    private static Event eventWithType(EventType eventType) {
        Event event = mock(Event.class);
        when(event.getKey()).thenReturn(eventType);

        return event;
    }
}
