package com.atlassian.johnson.event;

import java.util.function.Supplier;

import org.junit.After;
import org.junit.Test;

import com.atlassian.johnson.Johnson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static com.atlassian.johnson.event.EventLevel.*;

public class EventLevelsTest {

    @After
    public void terminateJohnson() {
        Johnson.terminate();
    }

    @Test
    public void testShouldBeAbleToGetErrorLevelIfJohnsonInitialized() {
        assertLevel(EventLevels::error, ERROR);
    }

    @Test
    public void testShouldBeAbleToGetFatalLevelIfJohnsonInitialized() {
        assertLevel(EventLevels::fatal, FATAL);
    }

    @Test
    public void testShouldBeAbleToGetWarningLevelIfJohnsonInitialized() {
        assertLevel(EventLevels::warning, WARNING);
    }

    @Test(expected = IllegalStateException.class)
    public void testShouldNotBeAbleToGetFatalLevelIfJohnsonNotInitialized() {
        Johnson.terminate();

        EventLevels.fatal();
    }

    private static void assertLevel(Supplier<EventLevel> eventLevelSupplier, String expectedLevelName) {
        Johnson.initialize("test-johnson-config.xml");

        EventLevel level = eventLevelSupplier.get();
        assertNotNull(level);
        assertEquals(level.getLevel(), expectedLevelName);
    }
}
