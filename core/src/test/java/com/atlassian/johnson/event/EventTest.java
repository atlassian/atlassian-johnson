package com.atlassian.johnson.event;

import java.io.PrintStream;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

public class EventTest {

    @Test
    public void testToStringDoesNotWriteToSystemOut() throws Exception {
        PrintStream realOut = System.out;

        PrintStream mockOut = mock(PrintStream.class);
        System.setOut(mockOut);
        try {
            Event event = new Event(new EventType("foo", "bar"), "fubar");
            assertNotNull(event.toString());
        } finally {
            try {
                verifyZeroInteractions(mockOut);
            } finally {
                System.setOut(realOut);
            }
        }
    }
}
