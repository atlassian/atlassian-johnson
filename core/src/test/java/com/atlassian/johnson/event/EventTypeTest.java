package com.atlassian.johnson.event;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.config.ConfigurationJohnsonException;

import static org.junit.Assert.assertEquals;

public class EventTypeTest {

    @BeforeClass
    public static void initialize() {
        Johnson.initialize("test-johnson-config.xml");
    }

    @AfterClass
    public static void terminate() {
        Johnson.terminate();
    }

    @Test
    public void testEventType() {
        EventType type = new EventType("foo", "bar");
        assertEquals("foo", type.getType());
        assertEquals("bar", type.getDescription());
    }

    @Test
    public void testGetEventType() throws ConfigurationJohnsonException {
        EventType expectedWarning = new EventType("database", "Database");
        assertEquals(expectedWarning, EventType.get("database"));
    }
}
