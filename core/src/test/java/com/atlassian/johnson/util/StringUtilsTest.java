package com.atlassian.johnson.util;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class StringUtilsTest {

    @Test
    public void testIsBlankReturnsTrueWhenBlank() {
        assertTrue(StringUtils.isBlank(null));
        assertTrue(StringUtils.isBlank(""));
        assertTrue(StringUtils.isBlank("   "));
        assertTrue(StringUtils.isBlank(" "));
    }

    @Test
    public void testIsBlankReturnsFalseWhenNotBlank() {
        assertFalse(StringUtils.isBlank(" ."));
        assertFalse(StringUtils.isBlank(" asd  a"));
    }

    @Test
    public void testIsEmptyReturnsTrueWhenEmpty() {
        assertTrue(StringUtils.isEmpty(null));
        assertTrue(StringUtils.isEmpty(""));
    }

    @Test
    public void testIsEmptyReturnsFalseWhenNotEmpty() {
        assertFalse(StringUtils.isEmpty(" ."));
        assertFalse(StringUtils.isEmpty(" asd  a"));
        assertFalse(StringUtils.isEmpty("   "));
    }

    @Test
    public void testDefaultIfEmptyDefaultsWhenEmpty() {
        // Setup
        String defaultValue = "defaultValue";

        // Exercise
        assertThat(StringUtils.defaultIfEmpty(null, defaultValue), is(defaultValue));
        assertThat(StringUtils.defaultIfEmpty("", defaultValue), is(defaultValue));
    }

    @Test
    public void testDefaultIfEmptyDoesNotDefaultWhenNotEmpty() {
        // Setup
        String defaultValue = "defaultValue";
        String realValue = "realValue";

        // Exercise
        assertThat(StringUtils.defaultIfEmpty(realValue, defaultValue), is(realValue));
        assertThat(StringUtils.defaultIfEmpty("    ", defaultValue), is("    "));
    }
}
