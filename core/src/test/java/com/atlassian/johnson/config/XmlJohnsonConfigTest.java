package com.atlassian.johnson.config;

import org.junit.Test;

import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.johnson.test.SimpleApplicationEventCheck;
import com.atlassian.johnson.test.SimpleEventCheck;
import com.atlassian.johnson.test.SimpleRequestEventCheck;
import com.atlassian.johnson.test.SimpleSetupConfig;

import static org.junit.Assert.*;

public class XmlJohnsonConfigTest {

    @Test(expected = ConfigurationJohnsonException.class)
    public void testBadEventCheck() {
        XmlJohnsonConfig.fromFile("test-johnson-config-badeventcheck.xml");
    }

    @Test(expected = ConfigurationJohnsonException.class)
    public void testBadEventCheckId() {
        XmlJohnsonConfig.fromFile("test-johnson-config-badid.xml");
    }

    @Test(expected = ConfigurationJohnsonException.class)
    public void testDuplicateEventCheckId() {
        XmlJohnsonConfig.fromFile("test-johnson-config-duplicateid.xml");
    }

    @Test
    public void testFromFile() {
        XmlJohnsonConfig config = XmlJohnsonConfig.fromFile("test-johnson-config.xml");

        // parameters
        assertEquals("bar", config.getParams().get("foo"));
        assertEquals("bat", config.getParams().get("baz"));

        // setup config
        assertTrue(config.getSetupConfig() instanceof SimpleSetupConfig);

        // event checks
        assertEquals(3, config.getEventChecks().size());
        assertTrue(config.getEventChecks().get(0) instanceof SimpleEventCheck);

        assertEquals(1, config.getRequestEventChecks().size());
        assertTrue(config.getEventChecks().get(1) instanceof SimpleRequestEventCheck);

        assertEquals(1, config.getApplicationEventChecks().size());
        assertTrue(config.getEventChecks().get(2) instanceof SimpleApplicationEventCheck);

        assertTrue(config.getEventCheck(1) instanceof SimpleEventCheck);
        assertTrue(config.getEventCheck(2) instanceof SimpleRequestEventCheck);
        assertNull(config.getEventCheck(3));

        // setup and error paths
        assertEquals("/the/setup/path.jsp", config.getSetupPath());
        assertEquals("/the/error/path.jsp", config.getErrorPath());

        // ignore paths
        assertEquals(2, config.getIgnorePaths().size());
        assertTrue(config.getIgnorePaths().contains("/ignore/path/1.jsp"));
        assertTrue(config.getIgnorePaths().contains("/ignore/path/*.html"));

        // some ignore mapping tests
        assertTrue(config.isIgnoredPath("/ignore/path/1.jsp"));
        assertTrue(config.isIgnoredPath("/ignore/path/2.html"));
        assertTrue(config.isIgnoredPath("/ignore/path/foo.html"));
        assertFalse(config.isIgnoredPath("/ignore/path"));

        // event levels
        EventLevel expectedError = new EventLevel("error", "Error");
        assertEquals(expectedError, config.getEventLevel("error"));
        EventLevel expectedWarning = new EventLevel("warning", "This is a warning buddy");
        assertEquals(expectedWarning, config.getEventLevel("warning"));

        // event types
        EventType expectedDatabase = new EventType("database", "Database");
        assertEquals(expectedDatabase, config.getEventType("database"));
        EventType expectedUpgrade = new EventType("upgrade", "Upgrade");
        assertEquals(expectedUpgrade, config.getEventType("upgrade"));
    }
}
