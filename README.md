# Johnson #

Johnson is an application consistency framework.

Johnson's primary interaction point is [Johnson.java](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/Johnson.java), which provides methods to
initialise the framework and accessors for the [configuration](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/config/JohnsonConfig.java)
and [event container](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/DefaultJohnsonEventContainer.java) which are available after initialisation.


The [config package](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/config/) contains a standard configuration interface as well as a default implementation which loads configuration from an XML file.

The [context package](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/context/) contains a [ServletContextListener](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/context/JohnsonContextListener.java) which can be wired into a web application to initialise and terminate Johnson as part of the web application's lifecycle. This is the preferred way to manage the framework's lifecycle.

The [filters package](https://bitbucket.org/atlassian/atlassian-johnson/src/master/core/src/main/java/com/atlassian/johnson/filters/) contains various servlet filters which can be wired into a web application to achieve different behaviours, such as redirecting users to a standard error page or returning a standard `503 Service Unavailable` error.

To contribute to this project, please see `CONTRIBUTING.md`.

This is currently maintained by the Lotus team -- contact on Slack #atst-dev

## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.

