package com.atlassian.johnson.spring.web.context;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.core.Conventions;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.johnson.spring.web.SpringEventType;

import static com.atlassian.johnson.spring.web.SpringEventType.createDefaultEvent;
import static com.atlassian.johnson.spring.web.SpringEventType.translateThrowable;

/**
 * Extends the standard Spring {@code ContextLoaderListener} to make it Johnson-aware. When using this class, if the
 * Spring context fails to start an event will added to Johnson rather than propagated to the servlet container.
 * <p>
 * The goal of this class is to prevent the web application from being shutdown if Spring cannot be started. By default,
 * if the {@code WebApplicationContext} cannot be started for any reason, an exception is thrown which is propagated up
 * to the container. When this happens, the entire web application is terminated. This precludes the use of Johnson,
 * which requires that the web application be up so that it can serve its status pages.
 */
public class JohnsonContextLoaderListener extends ContextLoaderListener {

    /**
     * The attribute added to the {@code ServletContext} when Spring context initialization is bypassed because a
     * previous {@link Event} indicates the application has already failed.
     *
     * @since 3.0
     */
    public static final String ATTR_BYPASSED =
            Conventions.getQualifiedAttributeName(JohnsonContextLoaderListener.class, "bypassed");

    private static final Logger LOG = LoggerFactory.getLogger(JohnsonContextLoaderListener.class);

    public JohnsonContextLoaderListener() {}

    public JohnsonContextLoaderListener(WebApplicationContext context) {
        super(context);
    }

    /**
     * Performs standard Spring {@code ContextLoaderListener} teardown and ensures any attributes added to the servlet
     * context for this dispatcher are removed.
     *
     * @param event the context event
     */
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        try {
            super.contextDestroyed(event);
        } finally {
            event.getServletContext().removeAttribute(ATTR_BYPASSED);
        }
    }

    /**
     * Overrides the standard Spring {@code ContextLoaderListener} initialisation to make it Johnson-aware, allowing it
     * to be automatically bypassed, when {@link com.atlassian.johnson.event.ApplicationEventCheck application checks}
     * produce {@link EventLevel#FATAL fatal} events, or add an {@link Event} if initialisation fails.
     * <p>
     * This implementation will never throw an exception. Unlike the base implementation, though, it may return
     * {@code null} if initialisation is bypassed (due to previous fatal events or failing to initialise) or if
     * initialisation fails.
     *
     * @param servletContext the servlet context
     * @return the initialised context, or {@code null} if initialisation is bypassed or fails
     */
    @Override
    public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
        String eventType = SpringEventType.getContextEventType(servletContext);

        // Search for previous FATAL errors and, if any are found, add another indicating Spring startup has been
        // canceled. The presence of other error types in the container (warnings, errors) will not prevent this
        // implementation from attempting to start Spring.
        JohnsonEventContainer container = Johnson.getEventContainer(servletContext);
        if (container.hasEvents()) {
            LOG.debug("Searching Johnson for previous {} errors", EventLevel.FATAL);
            for (Event event : container.getEvents()) {
                EventLevel level = event.getLevel();
                if (EventLevel.FATAL.equals(level.getLevel())) {
                    LOG.error(
                            "Bypassing Spring ApplicationContext initialisation; a previous {} error was found: {}",
                            level.getLevel(),
                            event.getDesc());
                    servletContext.setAttribute(ATTR_BYPASSED, Boolean.TRUE);

                    if (SpringEventType.addEventOnBypass(servletContext)) {
                        String message = "The Spring WebApplicationContext will not be started due to a previous "
                                + level.getLevel() + " error";
                        container.addEvent(new Event(EventType.get(eventType), message, level));
                    }

                    // The base class actually ignores the return, so null is safe.
                    return null;
                }
            }
        }

        WebApplicationContext context = null;
        try {
            LOG.debug("Attempting to initialise the Spring ApplicationContext");
            context = super.initWebApplicationContext(servletContext);
        } catch (Throwable t) {
            String message = "The Spring WebApplicationContext could not be started";
            LOG.error(message, t);

            // The Spring ContextLoader class sets the exception that was thrown during initialisation on the servlet
            // context under this constant. Whenever things attempt to retrieve the WebApplicationContext from the
            // servlet context, if the property value is an exception, it is rethrown. This makes other parts of the
            // web application, like DelegatingFilterProxies, fail to start (which, in turn, brings down the entire
            // web application and prevents access to Johnson).
            // Because we need the web application to be able to come up even if Spring fails, that behaviour is not
            // desirable. So before we add the event to Johnson, the first thing we do is clear that attribute back
            // off of the context. That way, when things attempt to retrieve the context, they'll just get a null back
            // (which matches what happens if we bypass Spring startup completely, above)
            servletContext.removeAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

            // After we remove Spring's attribute, we need to set our own. This allows other Johnson-aware constructs
            // to know we've bypassed Spring initialisation (or, more exactly, that it failed)
            Event event = translateThrowable(servletContext, t); // First apply EventExceptionTranslators, if set
            if (event == null) {
                event = createEvent(eventType, message, t); // For 2.x compatibility, try createEvent
                //noinspection ConstantConditions
                if (event == null) {
                    // When derived classes misbehave creating the event, apply a default
                    event = createDefaultEvent(eventType, message, t);
                }
            }
            servletContext.setAttribute(ATTR_BYPASSED, event); // event is never null by this point

            // Add the event to Johnson
            container.addEvent(event);
        }
        return context;
    }

    /**
     * May be overridden in derived classes to allow them to override the default event type or message based on
     * application-specific understanding of the exception that was thrown.
     * <p>
     * For cases where derived classes are not able to offer a more specific event type or message, they are
     * encouraged to fall back on the behaviour of this superclass method.
     *
     * @param defaultEventType the default event type to use if no more specific type is appropriate
     * @param defaultMessage   the default message to use if no more specific message is available
     * @param t                the exception thrown while attempting to initialise the WebApplicationContext
     * @return the event to add to Johnson, which may not be {@code null}
     */
    @Nonnull
    protected Event createEvent(
            @Nonnull String defaultEventType, @Nonnull String defaultMessage, @Nonnull Throwable t) {
        return createDefaultEvent(defaultEventType, defaultMessage, t);
    }
}
