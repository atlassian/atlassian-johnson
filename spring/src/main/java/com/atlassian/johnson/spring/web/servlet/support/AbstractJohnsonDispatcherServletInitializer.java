package com.atlassian.johnson.spring.web.servlet.support;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractDispatcherServletInitializer;

import com.atlassian.johnson.context.JohnsonContextListener;
import com.atlassian.johnson.spring.web.context.JohnsonContextLoaderListener;
import com.atlassian.johnson.spring.web.servlet.JohnsonDispatcherServlet;

/**
 * Extends Spring's {@code AbstractDispatcherServletInitializer} to use Johnson-aware components.
 * <ul>
 * <li>A {@link JohnsonContextListener} will be registered <i>before</i> any other listeners that are registered
 * by this initializer</li>
 * <li>A {@link JohnsonContextLoaderListener} will be used to initialize the {@link #createRootApplicationContext()
 * root ApplicationContext}, if one is created</li>
 * <li>A {@link JohnsonDispatcherServlet} will be used to initialize the {@link #createServletApplicationContext()
 * servlet ApplicationContext}</li>
 * </ul>
 * <p>
 * In addition to using Johnson-aware components by default, this base class allows derived initializers to override
 * {@link #createContextLoaderListener(WebApplicationContext) the ContextLoaderListener} and
 * {@link #createDispatcherServlet(WebApplicationContext) DispatcherServlet} types used. This is intended to allow
 * for application-specific handling on top of the Johnson-aware handling.
 *
 * @since 3.0
 */
public abstract class AbstractJohnsonDispatcherServletInitializer extends AbstractDispatcherServletInitializer {

    /**
     * Creates a {@link JohnsonContextLoaderListener} which will initialize and terminate the provided
     * {@code WebApplicationContext}. This method is provided as a convenience for derived classes to
     * simplify replacing the listener used.
     *
     * @param context the {@code WebApplicationContext} to be initialized by the created listener
     * @return the listener to register with the {@code ServletContext}
     */
    protected ContextLoaderListener createContextLoaderListener(WebApplicationContext context) {
        return new JohnsonContextLoaderListener(context);
    }

    /**
     * Creates a {@link JohnsonDispatcherServlet}, which will initialize the SpringMVC context in a Johnson-aware
     * away. If SpringMVC initialization fails, the application will be locked.
     *
     * @param context the {@code WebApplicationContext} to be initialized by the created dispatcher
     * @return the servlet to register with the {@code ServletContext}
     */
    protected DispatcherServlet createDispatcherServlet(WebApplicationContext context) {
        return new JohnsonDispatcherServlet(context);
    }

    /**
     * {@link #registerJohnsonContextListener(ServletContext) Registers a} {@link JohnsonContextListener} and then
     * delegates to the superclass's {@code onStartup(ServletContext)} implementation.
     *
     * @param servletContext the {@code ServletContext} to initialize
     * @throws ServletException potentially thrown by the superclass {@code onStartup(ServletContext)} implementation
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        registerJohnsonContextListener(servletContext);

        super.onStartup(servletContext);
    }

    /**
     * Overrides {@code AbstractContextLoaderListener}'s {@code registerContextLoaderListener} to register a
     * {@link #createContextLoaderListener(WebApplicationContext) JohnsonContextLoaderListener} instead of the
     * standard Spring {@code ContextLoaderListener}.
     *
     * @param servletContext the {@code ServletContext} to register the {@link JohnsonContextLoaderListener} in
     */
    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        WebApplicationContext context = createRootApplicationContext();
        if (context == null) {
            logger.debug(
                    "No ContextLoaderListener registered, as createRootApplicationContext() did not return an application context");
        } else {
            servletContext.addListener(createContextLoaderListener(context));
        }
    }

    /**
     * Overrides {@code AbstractDispatcherServletInitializer}'s {@code registerDispatcherServlet(ServletContext)} to
     * register a {@link #createDispatcherServlet(WebApplicationContext) JohnsonDispatcherServlet} instead of the
     * standard Spring {@code DispatcherServlet}.
     *
     * @param servletContext the {@code ServletContext} to register the {@link JohnsonDispatcherServlet} in
     */
    @Override
    protected void registerDispatcherServlet(ServletContext servletContext) {
        String servletName = getServletName();
        Assert.hasLength(servletName, "getServletName() may not return empty or null");

        WebApplicationContext servletAppContext = createServletApplicationContext();
        Assert.notNull(
                servletAppContext,
                "createServletApplicationContext() did not return an application context for servlet [" + servletName
                        + "]");

        ServletRegistration.Dynamic registration =
                servletContext.addServlet(servletName, createDispatcherServlet(servletAppContext));
        Assert.notNull(
                registration,
                "Failed to register servlet with name '" + servletName
                        + "'. Check if there is another servlet registered under the same name.");

        registration.setAsyncSupported(isAsyncSupported());
        registration.setLoadOnStartup(1);
        registration.addMapping(getServletMappings());

        Filter[] filters = getServletFilters();
        if (!ObjectUtils.isEmpty(filters)) {
            for (Filter filter : filters) {
                registerServletFilter(servletContext, filter);
            }
        }

        customizeRegistration(registration);
    }

    /**
     * Registers an {@link JohnsonContextListener} in in the provided {@code ServletContext}. This listener ensures
     * Johnson is initialized and terminated with the application.
     * <p>
     * Note: Even if this method is called multiple times, with its default implementation the listener will only be
     * added <i>once</i>.
     *
     * @param servletContext the {@code ServletContext} to register the {@link JohnsonContextListener} in
     * @see JohnsonContextListener#register(ServletContext)
     */
    protected void registerJohnsonContextListener(ServletContext servletContext) {
        JohnsonContextListener.register(servletContext);
    }
}
