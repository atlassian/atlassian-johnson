package com.atlassian.johnson.spring.event;

import org.springframework.web.context.ServletContextAware;

import com.atlassian.johnson.event.JohnsonEventListener;

/**
 * Extends {@link JohnsonEventListener} to decorate it with {@code ServletContextAware}, which triggers automatic
 * wiring in Spring and Spring Boot.
 *
 * @since 3.3
 */
public class AwareJohnsonEventListener extends JohnsonEventListener implements ServletContextAware {

    public AwareJohnsonEventListener() {
        super(null);
    }
}
