package com.atlassian.johnson.spring.lifecycle;

import java.io.IOException;
import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.johnson.spring.web.context.support.JohnsonHttpRequestHandlerServlet;

/**
 * A specialized {@link JohnsonHttpRequestHandlerServlet} for use with the {@link LifecycleDispatcherServlet}. This
 * servlet automatically redirects to the error page when requests are received during application startup.
 *
 * @since 3.0
 */
public class LifecycleHttpRequestHandlerServlet extends JohnsonHttpRequestHandlerServlet {

    private volatile boolean starting = true;

    @Override
    public void init() throws ServletException {
        LifecycleState state = LifecycleUtils.getCurrentState(getServletContext());
        if (state == LifecycleState.STARTED) {
            starting = false;

            super.init();
        } else {
            // If we don't call init() here, it will never be called, so we need to apply any ServletConfig
            // parameters here before returning
            applyParameters();
        }
    }

    @Override
    protected void service(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response)
            throws ServletException, IOException {
        if (starting) {
            if (LifecycleUtils.isStarting(getServletContext())) {
                // Otherwise, if the application is still starting, redirect the request to the error page
                sendRedirect(response);

                return;
            } else {
                // The first time we see a status other than CREATED or STARTING, note that the application
                // is no longer starting and service the request normally
                starting = false;
            }
        }

        super.service(request, response);
    }
}
