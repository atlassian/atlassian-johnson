package com.atlassian.johnson.spring.web.servlet;

import javax.annotation.Nonnull;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.springframework.core.Conventions;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.johnson.spring.web.SpringEventType;
import com.atlassian.johnson.spring.web.context.JohnsonContextLoaderListener;

import static com.atlassian.johnson.spring.web.SpringEventType.createDefaultEvent;
import static com.atlassian.johnson.spring.web.SpringEventType.translateThrowable;

/**
 * Extends the standard Spring {@code DispatcherServlet} to make it Johnson-aware. When using this class, if the root
 * Spring context fails to start the dispatcher will not attempt to create/start its child context.
 * <p>
 * The goal of this class is to prevent the web application from being shutdown if SpringMVC cannot be started. By
 * default, if the dispatcher's {@code WebApplicationContext} cannot be started for any reason, an exception is thrown
 * which is propagated up to the container. When this happens, the entire web application is terminated. This precludes
 * the use of Johnson, which requires that the web application be up so that it can serve its status pages.
 */
public class JohnsonDispatcherServlet extends DispatcherServlet {

    /**
     * The prefix for the attribute added to the {@code ServletContext} when Spring MVC initialization is bypassed
     * because a previous {@link Event} indicates the application has already failed. The MVC dispatcher's name is
     * appended to this prefix, separated by a colon, to form the attribute.
     *
     * @see #getBypassedAttributeName()
     * @since 3.0
     */
    public static final String PREFIX_BYPASSED =
            Conventions.getQualifiedAttributeName(JohnsonDispatcherServlet.class, "bypassed");

    private static final Logger LOG = LoggerFactory.getLogger(JohnsonDispatcherServlet.class);

    private String servletContextAttributeName;

    public JohnsonDispatcherServlet() {}

    public JohnsonDispatcherServlet(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    /**
     * Performs standard SpringMVC {@code DispatcherServlet} teardown and ensures any attributes added to the servlet
     * context for this dispatcher are removed.
     */
    @Override
    public void destroy() {
        try {
            super.destroy();
        } finally {
            getServletContext().removeAttribute(getBypassedAttributeName());
        }
    }

    /**
     * @return the attribute name which should be used to register the dispatcher servlet with the
     *         {@code ServletContext} to allow it to be retrieved later
     * @since 3.3
     */
    @Override
    public String getServletContextAttributeName() {
        if (servletContextAttributeName == null) {
            return getDefaultServletContextAttributeName();
        }
        return servletContextAttributeName;
    }

    /**
     * @param servletContextAttributeName the attribute name to use to register the dispatcher servlet with the
     *                                    {@code ServletContext}
     * @since 3.3
     */
    @SuppressWarnings("unused") // Set via Reflection by HttpServletBean.init
    public void setServletContextAttributeName(String servletContextAttributeName) {
        this.servletContextAttributeName = servletContextAttributeName;
    }

    /**
     * May be overridden in derived classes to allow them to override the default event type or message based on
     * application-specific understanding of the exception that was thrown.
     * <p>
     * For cases where derived classes are not able to offer a more specific event type or message, they are
     * encouraged to fall back on the behaviour of this superclass method.
     *
     * @param defaultEventType the default event type to use if no more specific type is appropriate
     * @param defaultMessage   the default message to use if no more specific message is available
     * @param t                the exception thrown while attempting to initialise the WebApplicationContext
     * @return the event to add to Johnson, which may not be {@code null}
     */
    @Nonnull
    protected Event createEvent(
            @Nonnull String defaultEventType, @Nonnull String defaultMessage, @Nonnull Throwable t) {
        return createDefaultEvent(defaultEventType, defaultMessage, t);
    }

    /**
     * Allows derived classes to override the name of the attribute which is added to the {@code ServletContext} when
     * this dispatcher is bypassed (whether due to the {@link JohnsonContextLoaderListener} being bypassed or due to
     * the initialisation of the dispatcher failing).
     * <p>
     * The default attribute name is: "{@link #PREFIX_BYPASSED}:{@code getServletName()}"
     *
     * @return the name for the bypassed context attribute
     */
    @Nonnull
    protected String getBypassedAttributeName() {
        return PREFIX_BYPASSED + ":" + getServletName();
    }

    /**
     * @return the default attribute name to use to register the dispatcher servlet with the {@code ServletContext},
     *         which will be used if an explicit name is not {@link #setServletContextAttributeName set}
     * @since 3.3
     */
    protected String getDefaultServletContextAttributeName() {
        return super.getServletContextAttributeName();
    }

    /**
     * Overrides the standard SpringMVC {@code DispatcherServlet} initialisation to make it Johnson-aware, allowing it
     * to be automatically bypassed, when paired with a {@link JohnsonContextLoaderListener}, or add an {@link Event}
     * if initialisation fails.
     * <p>
     * This implementation will never throw an exception. Unlike the base implementation, though, it may return
     * {@code null} if initialisation is bypassed (due to the main {@code WebApplicationContext} being bypassed
     * or failing to initialise) or if initialisation fails.
     *
     * @return the initialised context, or {@code null} if initialisation is bypassed or fails
     */
    @Override
    protected WebApplicationContext initWebApplicationContext() {
        ServletConfig servletConfig = getServletConfig();
        ServletContext servletContext = getServletContext();
        JohnsonEventContainer container = Johnson.getEventContainer(servletContext);
        String eventType = SpringEventType.getServletEventType(servletConfig);

        Object attribute = servletContext.getAttribute(JohnsonContextLoaderListener.ATTR_BYPASSED);
        // First, check to see if the WebApplicationContext was bypassed. If it was, it's possible, based on
        // configuration, that no event was added. However, we must bypass SpringMVC initialisation as well,
        // because no parent context will be available.
        if (Boolean.TRUE == attribute) // Fully bypassed, without even trying to start
        {
            LOG.error(
                    "Bypassing SpringMVC dispatcher [{}] initialisation; Spring initialisation was bypassed",
                    getServletName());
            servletContext.setAttribute(getBypassedAttributeName(), Boolean.TRUE);

            if (SpringEventType.addEventOnBypass(servletConfig)) {
                String message = "SpringMVC dispatcher [" + getServletName()
                        + "] will not be started because the Spring WebApplicationContext was not started";
                container.addEvent(new Event(EventType.get(eventType), message, EventLevel.get(EventLevel.FATAL)));
            }

            return null;
        }
        // If WebApplicationContext initialisation wasn't bypassed, check to see if it failed. SpringMVC initialisation
        // is guaranteed to fail if the primary WebApplicationContext failed, so we'll want to bypass it.
        if (attribute instanceof Event) {
            Event event = (Event) attribute;

            LOG.error(
                    "Bypassing SpringMVC dispatcher [{}] initialisation; Spring initialisation failed: {}",
                    getServletName(),
                    event.getDesc());
            servletContext.setAttribute(getBypassedAttributeName(), Boolean.TRUE);

            if (SpringEventType.addEventOnBypass(servletConfig)) {
                String message = "SpringMVC dispatcher [" + getServletName()
                        + "] will not be started because the Spring WebApplicationContext failed to start";
                container.addEvent(new Event(EventType.get(eventType), message, event.getLevel()));
            }

            return null;
        }

        // If we make it here, the Spring WebApplicationContext should have started successfully. That means it's safe
        // to try and start this SpringMVC dispatcher.
        WebApplicationContext context = null;
        try {
            LOG.debug("Attempting to initialise the Spring ApplicationContext");
            context = super.initWebApplicationContext();
        } catch (Throwable t) {
            String message = "SpringMVC dispatcher [" + getServletName() + "] could not be started";
            LOG.error(message, t);

            Event event = translateThrowable(getServletConfig(), t); // First apply EventExceptionTranslators, if set
            if (event == null) {
                event = createEvent(eventType, message, t); // For 2.x compatibility, try createEvent
                //noinspection ConstantConditions
                if (event == null) {
                    // When derived classes misbehave creating the event, apply a default
                    event = createDefaultEvent(eventType, message, t);
                }
            }
            servletContext.setAttribute(getBypassedAttributeName(), event);

            container.addEvent(event);
        }
        return context;
    }
}
