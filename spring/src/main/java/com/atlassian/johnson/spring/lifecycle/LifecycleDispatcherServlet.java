package com.atlassian.johnson.spring.lifecycle;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.spring.web.servlet.JohnsonDispatcherServlet;

/**
 * Initializes SpringMVC on a separate thread, allowing the web application to handle basic requests while the
 * application is still starting.
 * <p>
 * SpringMVC is started <i>standalone</i> by this servlet. It is assumed that no {@code ContextLoaderListener}
 * has been registered and prepared a root {@code WebApplicationContext} and that the SpringMVC context should
 * be used as the root.
 * <p>
 * When using this servlet, {@link LifecycleDelegatingFilterProxy} and {@link LifecycleHttpRequestHandlerServlet}
 * should be used in place of Spring's {@code DelegatingFilterProxy} and {@code HttpRequestHandlerServlet}. The
 * lifecycle versions automatically defer attempting to bind to their associated beans until the application has
 * {@link LifecycleState#STARTED started}.
 *
 * @since 3.0
 */
@UnrestrictedAccess
public class LifecycleDispatcherServlet extends HttpServlet {

    private static final String PROP_SYNCHRONOUS = "johnson.spring.lifecycle.synchronousStartup";

    private final Logger log = LoggerFactory.getLogger(getClass());

    private volatile DispatcherServlet delegate;
    private volatile Thread startup;

    @Override
    public void destroy() {
        Thread startup = this.startup; // Shadowing intentional
        if (startup != null) {
            if (startup.isAlive()) {
                // Try to interrupt Spring startup so that the server can terminate
                startup.interrupt();
            }

            try {
                // Wait for Spring startup to either complete or bomb out due to being interrupted. Note that,
                // because Spring may still complete its startup despite our attempt to interrupt it, we check
                // the delegate DispatcherServlet after cleaning up the startup thread
                startup.join();
            } catch (InterruptedException e) {
                log.error("The SpringMVC startup thread could not be joined", e);
            }
        }

        DispatcherServlet delegate = this.delegate; // Shadowing intentional
        if (delegate != null) {
            // Shut down Spring
            delegate.destroy();
        }
    }

    @Override
    public void init() throws ServletException {
        Thread thread = new Thread(() -> {
            JohnsonDispatcherServlet servlet = new JohnsonDispatcherServlet() {

                @Override
                protected String getDefaultServletContextAttributeName() {
                    // Unless an explicit attribute was requested, publish our context as the root web
                    // application context. This emulates ContextLoaderListener and is required so that
                    // DelegatingFilterProxy and HttpRequestHandlerServlet instances can their beans
                    return WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE;
                }
            };

            LifecycleUtils.updateState(getServletContext(), LifecycleState.STARTING);
            try {
                // This allows webapps to configure the LifecycleDispatcherServlet as if it was the SpringMVC
                // DispatcherServlet, accepting all the same parameters, without this class having to manually
                // copy them all over
                servlet.init(getServletConfig());

                delegate = servlet; // If we make it here, SpringMVC has started successfully. Enable the delegate
                LifecycleUtils.updateState(getServletContext(), LifecycleState.STARTED);
            } catch (Exception e) {
                LifecycleUtils.updateState(getServletContext(), LifecycleState.FAILED);
                log.error("SpringMVC could not be started", e);
            } finally {
                startup = null; // We're done running, so remove our reference
            }
        });
        thread.setDaemon(true);
        thread.setName("spring-startup");

        (startup = thread).start();

        if (Boolean.getBoolean(PROP_SYNCHRONOUS)) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.warn("Interrupted while waiting for synchronous Spring startup to complete");
            }
        }
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DispatcherServlet delegate = this.delegate; // Shadowing intentional
        if (delegate == null) {
            JohnsonConfig config = Johnson.getConfig(getServletContext());

            String nextUrl = request.getRequestURI();

            if (request.getQueryString() != null && !request.getQueryString().isEmpty()) {
                nextUrl += "?" + request.getQueryString();
            }

            String redirectUrl =
                    request.getContextPath() + config.getErrorPath() + "?next=" + URLEncoder.encode(nextUrl, "UTF-8");

            response.sendRedirect(redirectUrl);
        } else {
            delegate.service(request, response);
        }
    }
}
