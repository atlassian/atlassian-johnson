# Status

This project is open-source, and has [one Atlassian-owned repository](https://bitbucket.org/atlassian/atlassian-johnson).

# Ownership

The Johnson project [has no official owner or maintainer](https://extranet.atlassian.com/display/DEV/Atlassian+Johnson).

# Contributors

Contributors are typically developers of Atlassian products that use this framework.

# Coding Standards

* Use the [Bitbucket Server coding style](https://extranet.atlassian.com/display/BSERV/Bitbucket+Server+Engineering+Practices).
* For aspects not codified therein, follow the project's existing style, e.g. name JUnit test methods `testBlah`.
* Write (or update) unit tests for code that you add (or change), to the extent possible.

# Workflow

1. Raise a JIRA issue for each change, preferably [on JAC](https://jira.atlassian.com/projects/JOHNSON/summary)
where it's visible to the public.
1. Create a branch from `master` for your issue, prefixed with your issue key, e.g. `JOHNSON-123-fix-thing`.
1. Make your desired changes, observing the above coding standards.
1. Bump the version number if necessary, observing the rules of [semver](http://semver.org/).
1. Open a pull request, choosing at least two reviewers who are able to comment meaningfully on your contribution,
e.g. recent committers or other developers who understand the change you are trying to make.
1. Obtain approval from at least two of those reviewers.
1. Ensure the tests pass locally by running `mvn clean install` (this is a stopgap until the project has a CI build).
1. Merge your PR to `master` and delete your feature branch.

# Releasing

1. Ensure that the POMs contain the desired release number, but with the `-SNAPSHOT` suffix.
1. Run the [release plan](https://server-syd-bamboo.internal.atlassian.com/browse/FUS-JR).